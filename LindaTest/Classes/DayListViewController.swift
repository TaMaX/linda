//
//  DayListViewController.swift
//  LindaTest
//
//  Created by Mateusz Ramski on 19.07.2017.
//  Copyright © 2017 Mateusz Ramski. All rights reserved.
//

import UIKit

class DayListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    let dayCellIdentifier = "dayCell"
    let filtersSegueIdentifier = "toFilters"
    let detailsSegueIdentifier = "toDetails"
    
    var viewModel:DayListViewModel?{
        didSet{
            self.viewModel?.weeks.bind{[weak self] days in
                DispatchQueue.main.async {
                    self?.tableView?.reloadData()
                }
            }
            self.viewModel?.error.bind{[weak self] error in
                self?.showAlert(text: error)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UINib(nibName: "DayTableViewCell", bundle: nil), forCellReuseIdentifier: dayCellIdentifier)
        self.tableView.estimatedRowHeight = 50
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.viewModel = DayListViewModel()
        self.viewModel?.loadDays()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let selectedIndexPath = self.tableView.indexPathForSelectedRow{
            self.tableView.deselectRow(at: selectedIndexPath, animated: true)
        }
    }
    
    @IBAction func filtersButtonTapped(_ sender: UIBarButtonItem) {
        self.performSegue(withIdentifier: self.filtersSegueIdentifier, sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == self.filtersSegueIdentifier {
            let viewController = ((segue.destination as! UINavigationController).viewControllers.first) as! FiltersViewController
            viewController.viewModel = self.viewModel?.filtersViewModel
        }
        
        if segue.identifier == self.detailsSegueIdentifier {
            let viewController = segue.destination as! DayDetailsViewController
            viewController.viewModel = sender as? DayViewModel
        }
    }
    
    private func showAlert(text:String){
        let alertController = UIAlertController(title: "Error", message: text, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        present(alertController, animated: true, completion: nil)
    }
}

extension DayListViewController:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = self.viewModel?.weeks.value[indexPath.section].days[indexPath.row]
        self.performSegue(withIdentifier: self.detailsSegueIdentifier, sender: item)
    }
}

extension DayListViewController:UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModel?.weeks.value.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel?.weeks.value[section].days.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.dayCellIdentifier, for: indexPath) as! DayTableViewCell
        let item = self.viewModel?.weeks.value[indexPath.section].days[indexPath.row]
        cell.configure(viewModel: item!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.viewModel?.weeks.value[section].title ?? ""
    }
}
