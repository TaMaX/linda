//
//  Day.swift
//  LindaTest
//
//  Created by Mateusz Ramski on 19.07.2017.
//  Copyright © 2017 Mateusz Ramski. All rights reserved.
//

import Foundation
import Unbox

protocol Displayable {
    var displayName:String{get}
}

struct Day {
    
    enum DayListGroup:String {
        case week = "Week day"
        case month
        case seasonweek = "Season week"
        case season
    }
    
    enum Season:String, UnboxableEnum, Displayable{
        var displayName: String {
            return self.rawValue.capitalized
        }
        
        case ordinary
        case advent
        case christmas
    }
    
    enum Weakday:String, UnboxableEnum, Displayable{
        var displayName: String {
            return self.rawValue.capitalized
        }
        
        case monday
        case tuesday
        case wednesday
        case thursday
        case friday
        case saturday
        case sunday
    }
    
    enum Month:Int, Displayable {
        var displayName: String{
            return DateFormatter().monthSymbols[self.rawValue-1]
        }
        
        case january = 1
        case february
        case march
        case april
        case may
        case june
        case luly
        case august
        case september
        case october
        case november
        case december
    }
    
    struct SeasonWeek:Displayable, Hashable{
        var displayName: String{
            return "\(value)"
        }
        
        let value:Int
        
        var hashValue: Int {
            return value.hashValue ^ value.hashValue &* 16777619
        }
        static func == (lhs: SeasonWeek, rhs: SeasonWeek) -> Bool {
            return lhs.value == rhs.value
        }
    }
    
    let date:Date
    let month:Month
    let season:Season
    let seasonWeek:SeasonWeek
    let weekday:Weakday
    var celebrations = [Celebration]()
    
    static func loadDays(completion:(_ days:[Day], _ error:NSError?)->()){
        var days:[Day] = []
        do {
            if let file = Bundle.main.url(forResource: "data", withExtension: "json"){
                let data = try Data(contentsOf: file)
                days = try unbox(data: data)
                completion(days, nil)
            } else {
                let userInfo = [
                    NSLocalizedDescriptionKey :  NSLocalizedString("No file", value: "No file", comment: "") ,
                    NSLocalizedFailureReasonErrorKey : NSLocalizedString("Cant read file", value: "Cant read file", comment: "")
                ]
                let error = NSError(domain: "com.bytearray.LindaTest",code: 1001,userInfo:userInfo)
                completion(days,error)
            }
        } catch {
            completion(days,error as NSError)
        }
    }
}

extension Day:Unboxable{
    init(unboxer: Unboxer) throws {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
        let calendar = Calendar.current
        
        self.date = try unboxer.unbox(key: "date", formatter: dateFormatter)
        self.month = Month(rawValue: calendar.component(.month, from: self.date)) ?? .january
        self.season = try unboxer.unbox(key: "season")
        let seasonWeek:Int = try unboxer.unbox(key: "season_week")
        self.seasonWeek = SeasonWeek(value: seasonWeek)
        self.weekday = try unboxer.unbox(key: "weekday")
        self.celebrations = try unboxer.unbox(key: "celebrations")
    }
}
