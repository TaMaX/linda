//
//  Celebration.swift
//  LindaTest
//
//  Created by Mateusz Ramski on 19.07.2017.
//  Copyright © 2017 Mateusz Ramski. All rights reserved.
//

import Foundation
import Unbox

struct Celebration {
    
    enum Colour:String, UnboxableEnum{
        case white
        case red
        case green
        case violet
    }
    
    let title:String
    let colour:Colour
    let rank:String
    let rankNum:String
}

extension Celebration: Unboxable {
    init(unboxer: Unboxer) throws {
        self.title = try unboxer.unbox(key: "title")
        self.colour = unboxer.unbox(key: "colour") ?? .white
        self.rank = try unboxer.unbox(key: "rank")
        self.rankNum = try unboxer.unbox(key: "rank_num")
    }
}
