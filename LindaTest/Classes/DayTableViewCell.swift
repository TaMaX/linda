//
//  DayTableViewCell.swift
//  LindaTest
//
//  Created by Mateusz Ramski on 19.07.2017.
//  Copyright © 2017 Mateusz Ramski. All rights reserved.
//

import UIKit

class DayTableViewCell: UITableViewCell {

    
    @IBOutlet weak var dayName: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var season: UILabel!
    @IBOutlet weak var marker: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.marker.subviews.forEach{
            $0.removeFromSuperview()
        }
    }

    func configure(viewModel:DayViewModel){
        self.dayName.text = viewModel.weekday
        self.date.text = viewModel.date
        self.season.text = viewModel.season
        
        
        let width = self.marker.frame.size.width / CGFloat(viewModel.celebrations.count) - 4
        for (index, element) in viewModel.celebrations.enumerated() {
            let frame = CGRect(x: 0 + ((width + 4) * CGFloat(index)) , y: 0, width: width, height: self.marker.frame.size.height)
            let view = UIView(frame: frame)
            view.backgroundColor = element.color
            self.marker.addSubview(view)
        }
    }
    
}
