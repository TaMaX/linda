//
//  DayListViewModel.swift
//  LindaTest
//
//  Created by Mateusz Ramski on 19.07.2017.
//  Copyright © 2017 Mateusz Ramski. All rights reserved.
//

import Foundation


class DayListViewModel {
    
    struct Section {
        let title:String
        let days:[DayViewModel]
    }
    
    let weeks = Dynamic<[Section]>([])
    private var days = [Day]()
    private var filteredDays = [Day]()
    
    let error = Dynamic<String>("")
    
    lazy var filtersViewModel:FiltersViewModel = {
        let filtersViewModel = FiltersViewModel()
        
        filtersViewModel.filterColor = {[weak self] color in
            self?.filterByColour(colour: color)
        }
        
        filtersViewModel.groupBy = {[weak self] group in
            self?.group(by: group)
        }
        
        filtersViewModel.reset = {[weak self] in
            self?.resetFilter()
        }
        
        return filtersViewModel
    }()
    
    func loadDays(){
        Day.loadDays { (days, error) in
            if let error = error {
                self.error.value = error.localizedDescription
                return
            }
                        
            self.days = days
            self.filterByColour(colour: self.filtersViewModel.selectedColor)
            self.group(by: self.filtersViewModel.selectedGroup)
        }
    }
    
    private func group(by group:Day.DayListGroup){
        var grouppedDays = [AnyHashable:[Day]]()
        let title = "\(group.rawValue.capitalized) :"
        
        switch group {
        case .month:
            grouppedDays = self.filteredDays.categorise{ $0.month }
        case .week:
            grouppedDays = self.filteredDays.categorise{ $0.weekday }
        case .season:
            grouppedDays = self.filteredDays.categorise{ $0.season }
        case .seasonweek:
            grouppedDays = self.filteredDays.categorise{ $0.seasonWeek }
        }
        
        var sections = [Section]()
        grouppedDays.forEach{
            let dayModels = $0.value.map{DayViewModel(day: $0)}
            sections.append(Section(title: title + " " + (($0.key as? Displayable)?.displayName ?? ""), days: dayModels))
        }
        self.weeks.value = sections
    }
    
    private func filterByColour(colour:Celebration.Colour?){
        if let colour = colour {
            self.filteredDays = self.days.filter{day in
                for celebration in day.celebrations {
                    if celebration.colour == colour {
                        return true
                    }
                }
                return false
            }
            self.group(by: self.filtersViewModel.selectedGroup)
        } else {
            self.filteredDays = self.days
        }
    }
    
    private func resetFilter(){
        self.filteredDays = self.days
        self.group(by: self.filtersViewModel.selectedGroup)
    }
}
