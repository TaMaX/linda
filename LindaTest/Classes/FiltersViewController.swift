//
//  FiltersViewController.swift
//  LindaTest
//
//  Created by Mateusz Ramski on 21.07.2017.
//  Copyright © 2017 Mateusz Ramski. All rights reserved.
//

import UIKit

class FiltersViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var viewModel:FiltersViewModel?{
        didSet{
            self.viewModel?.items.bind{[weak self] items in
                DispatchQueue.main.async {
                    self?.tableView?.reloadData()
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func resetTapped(_ sender: UIBarButtonItem) {
        self.viewModel?.resetFilters()
    }
    
    @IBAction func cancelTapped(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension FiltersViewController:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let filter = self.viewModel?.items.value[indexPath.section].items[indexPath.row] else {return}
        self.viewModel?.filterSelected(filter: filter)
        self.dismiss(animated: true, completion: nil)
    }
}

extension FiltersViewController:UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModel?.items.value.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel?.items.value[section].items.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")!
        let item = self.viewModel?.items.value[indexPath.section].items[indexPath.row]
        cell.textLabel?.text = item?.displayName
        
       
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        var shouldBeSelected = false
        let item = self.viewModel!.items.value[indexPath.section].items[indexPath.row]
        switch item {
        case .color(let color):
            shouldBeSelected = self.viewModel!.selectedColor == color
        case .group(let group):
            shouldBeSelected = self.viewModel!.selectedGroup == group
        }
        
        cell.setSelected(shouldBeSelected, animated: false)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.viewModel?.items.value[section].title ?? ""
    }
}
