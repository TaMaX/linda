//
//  DayDetailsViewController.swift
//  LindaTest
//
//  Created by Mateusz Ramski on 21.07.2017.
//  Copyright © 2017 Mateusz Ramski. All rights reserved.
//

import UIKit

class DayDetailsViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    let celebrationCellIdentifier = "celebrationCell"
    let dayDetailsCellIdentifier = "dayDetailsCell"
    
    var viewModel:DayViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.register(UINib(nibName: "CelebrationTableViewCell", bundle: nil), forCellReuseIdentifier: celebrationCellIdentifier)
        self.tableView.register(UINib(nibName: "DayDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: dayDetailsCellIdentifier)
        self.tableView.estimatedRowHeight = 50
        self.tableView.rowHeight = UITableViewAutomaticDimension
    }
}

extension DayDetailsViewController:UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 1 : (self.viewModel?.celebrations.count ?? 0)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell
        
        if indexPath.section == 0 {
            cell = tableView.dequeueReusableCell(withIdentifier: self.dayDetailsCellIdentifier, for: indexPath)
            (cell as! DayDetailsTableViewCell).configure(viewModel: self.viewModel!)
            
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: self.celebrationCellIdentifier, for: indexPath) as! CelebrationTableViewCell
            let item = self.viewModel!.celebrations[indexPath.row]
            (cell as! CelebrationTableViewCell).configure(viewModel: item)
        }
        
        return cell
    }
}
