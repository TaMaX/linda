//
//  DayViewModel.swift
//  LindaTest
//
//  Created by Mateusz Ramski on 19.07.2017.
//  Copyright © 2017 Mateusz Ramski. All rights reserved.
//

import UIKit

class DayViewModel {
    
    let date:String
    let weekday:String
    let season:String
    let seasonWeek:String
    let celebrations:[CelebrationCellViewModel]
    
    init(day:Day) {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MMM-yyyy"
        
        self.weekday = day.weekday.rawValue.capitalized
        self.date = "Date: " + formatter.string(from: day.date)
        self.seasonWeek = "Season week: " + day.seasonWeek.displayName
        self.season = "Season: " + day.season.rawValue.capitalized
        
        var celebrationModels = [CelebrationCellViewModel]()
        day.celebrations.forEach{
            celebrationModels.append(CelebrationCellViewModel(celebration: $0))
        }
        self.celebrations = celebrationModels
    }
}
