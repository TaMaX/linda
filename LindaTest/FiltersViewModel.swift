//
//  FiltersViewModel.swift
//  LindaTest
//
//  Created by Mateusz Ramski on 21.07.2017.
//  Copyright © 2017 Mateusz Ramski. All rights reserved.
//

import Foundation

class FiltersViewModel{
    
    private(set) var selectedGroup:Day.DayListGroup = .month
    private(set) var selectedColor:Celebration.Colour?
    
    var filterColor:((Celebration.Colour) -> ())?
    var groupBy:((Day.DayListGroup) -> ())?
    var reset:(() -> ())?
    
    enum Filter{
        case group(Day.DayListGroup)
        case color(Celebration.Colour)
        
        var displayName: String {
            switch self {
            case .color(let color):
                return color.rawValue.capitalized
            case .group(let group):
                return group.rawValue.capitalized
            }
        }
    }
    
    struct Section{
        let title:String
        let items:[Filter]
    }
    
    let items = Dynamic<[Section]>([])
    
    init() {
        self.items.value = [
            Section(title: "Group by", items:
                [
                    .group(.week),
                    .group(.month),
                    .group(.season),
                    .group(.seasonweek)
                ]),
            Section(title: "Filter by", items:
                [
                    .color(.white),
                    .color(.red),
                    .color(.green),
                    .color(.violet)
                ])
        ]
    }
    
    func filterSelected(filter:Filter){
        switch filter {
        case .color(let color):
            self.selectedColor = color
            self.filterColor?(color)
        case .group(let group):
            self.selectedGroup = group
            self.groupBy?(group)
        }
    }
    
    func resetFilters(){
        self.selectedColor = nil
        self.selectedGroup = .month
        self.reset?()
        self.items.fire()
    }
}
