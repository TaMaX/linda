//
//  DayDetailsTableViewCell.swift
//  LindaTest
//
//  Created by Mateusz Ramski on 21.07.2017.
//  Copyright © 2017 Mateusz Ramski. All rights reserved.
//

import UIKit

class DayDetailsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var seasonLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var seasonWeekLabel: UILabel!
    

    func configure(viewModel:DayViewModel){
        self.seasonLabel.text = viewModel.season
        self.seasonWeekLabel.text = viewModel.seasonWeek
        self.dateLabel.text = viewModel.date
    }
    
}
