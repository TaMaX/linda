//
//  CelebrationTableViewCell.swift
//  LindaTest
//
//  Created by Mateusz Ramski on 21.07.2017.
//  Copyright © 2017 Mateusz Ramski. All rights reserved.
//

import UIKit

class CelebrationTableViewCell: UITableViewCell {

    @IBOutlet weak var rankLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
 
    func configure(viewModel:CelebrationCellViewModel){
        self.rankLabel.text = viewModel.rank
        self.titleLabel.text = viewModel.title
        self.contentView.backgroundColor = viewModel.color
    }
    
}
