//
//  CelebrationCellViewModel.swift
//  LindaTest
//
//  Created by Mateusz Ramski on 21.07.2017.
//  Copyright © 2017 Mateusz Ramski. All rights reserved.
//

import UIKit

class CelebrationCellViewModel{
    
    let rank:String
    let title:String
    let color:UIColor
    
    init(celebration:Celebration) {
        self.rank = "Rank: " + celebration.rank.capitalized
        self.title = celebration.title.capitalized
        
        switch celebration.colour {
        case .green:
            self.color = UIColor.green
        case .red:
            self.color = UIColor.red
        case .violet:
            self.color = UIColor.purple
        case .white:
            self.color = UIColor.white
        }
    }
}
